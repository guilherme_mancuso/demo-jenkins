import time
import sys

def memoization(funcao):
    resultados = {}
    def busca_fib(indice):
        if indice not in resultados:
            resultados[indice] = funcao(indice)
        return resultados[indice]
    return busca_fib

@memoization
def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-1) + fib(n-2)

start_time = time.time()
print fib(int(sys.argv[1]))
print ((time.time()) - start_time)

